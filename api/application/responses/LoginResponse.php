<?php

declare(strict_types=1);

namespace api\application\responses;

class LoginResponse
{

    public function __construct(
        public string $token
    ) {}
}