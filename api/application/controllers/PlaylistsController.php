<?php

declare(strict_types=1);

namespace api\application\controllers;

use api\domain\dto\PlaylistDTO;
use api\domain\dto\TrackDTO;
use api\domain\services\DtoBuilder;
use api\domain\services\EntityFinder;
use integrations\domain\models\PlaylistsModel;
use shared\domain\entity\PlaylistEntity;
use yii\base\Module;
use Throwable;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use shared\domain\exceptions\DomainException;

class PlaylistsController extends AbstractController
{
    public function __construct(
        string $id,
        Module $module,
        private DtoBuilder $builder,
        private EntityFinder $finder,
        private PlaylistsModel $playlistsModel,
    ) {
        parent::__construct($id, $module);
    }

    /**
     * @return array<string, string[]>
     */
    public function getVerbsActions(): array
    {
        return [
            'index' => ['GET'],
            'next-track' => ['GET'],
            'prev-track' => ['GET'],
        ];
    }

    /**
     * /playlists
     * @return PlaylistDTO[]
     */
    public function actionIndex(): array
    {
        return array_map(
            fn (PlaylistEntity $playlistEntity) => $this->builder->getPlaylistDTO($playlistEntity),
            $this->finder->getAllPlaylists()
        );
    }

    /**
     * /playlists/$playlist/tracks/next
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function actionNextTrack(int $playlist): TrackDTO
    {
        $playlistEntity = $this->finder->getPlaylist($playlist);
        $trackEntity = $this->playlistsModel->getNextTrack($playlistEntity);
        return $this->builder->getTrackDTO($trackEntity);
    }

    /**
     * /playlists/$playlist/tracks/prev
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function actionPrevTrack(int $playlist): TrackDTO
    {
        // TODO
        // Реализовать логику сохранения списка прослушанных треков
        // и на её основе отдавать треки.
        // Ну а пока так:

        $playlistEntity = $this->finder->getPlaylist($playlist);
        $trackEntity = $this->playlistsModel->getNextTrack($playlistEntity);
        return $this->builder->getTrackDTO($trackEntity);
    }
}