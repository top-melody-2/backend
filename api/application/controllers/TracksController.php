<?php

declare(strict_types=1);

namespace api\application\controllers;

use api\domain\services\DtoBuilder;
use api\domain\services\EchoStreamService;
use api\domain\services\EntityFinder;
use integrations\domain\models\TracksModel;
use yii\base\Module;
use Throwable;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use shared\domain\exceptions\DomainException;

class TracksController extends AbstractController
{
    public function __construct(
        string $id,
        Module $module,
        private EntityFinder $finder,
        private TracksModel $tracksModel,
        private EchoStreamService $echoStreamService,
    ) {
        parent::__construct($id, $module);
    }

    /**
     * @return array<string, string[]>
     */
    protected function getVerbsActions(): array
    {
        return [
            'stream' => ['GET'],
            'like' => ['PATCH'],
            'dislike' => ['PATCH'],
        ];
    }

    /**
     * /tracks/$track/stream
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function actionStream(int $track): void
    {
        $trackEntity = $this->finder->getTrack($track);
        $stream = $this->tracksModel->getStream($trackEntity);
        $this->echoStreamService->service($stream);
    }

    /**
     * /tracks/$track/like
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function actionLike(int $track): string
    {
        $trackEntity = $this->finder->getTrack($track);
        return $this->tracksModel->like($trackEntity);
    }

    /**
     * /tracks/$track/dislike
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function actionDislike(int $track): string
    {
        $trackEntity = $this->finder->getTrack($track);
        return $this->tracksModel->dislike($trackEntity);
    }
}