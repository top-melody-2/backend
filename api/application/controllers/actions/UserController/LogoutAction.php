<?php

declare(strict_types=1);

namespace api\application\controllers\actions\UserController;

use api\application\controllers\actions\AbstractAction;
use api\domain\models\JWTModel;
use yii\web\Controller;
use api\domain\exceptions\UserException;
use shared\domain\exceptions\DomainException;

class LogoutAction extends AbstractAction
{

    public function __construct(
        string $id,
        Controller $controller,
        private JWTModel $jwtModel,
        array $config = []
    ) {
        parent::__construct($id, $controller, $config);
    }

    /**
     * /user/logout
     * @throws UserException
     * @throws DomainException
     */
    public function run(): void
    {
        $jwt = $this->jwtModel->getCurrent();
        $this->jwtModel->recall($jwt);
    }
}