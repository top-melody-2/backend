<?php

declare(strict_types=1);

namespace api\application\controllers\actions\UserController;

use api\application\controllers\actions\AbstractAction;
use api\application\requests\UserRequest;
use api\application\responses\LoginResponse;
use api\domain\models\JWTModel;
use shared\domain\exceptions\DomainException;
use shared\domain\models\UserModel;
use shared\domain\repository\UserRepositoryInterface;
use yii\web\Controller;

class LoginAction extends AbstractAction
{

    public function __construct(
        string $id,
        Controller $controller,
        private UserRequest $userRequest,
        private UserModel $userModel,
        private JWTModel $jwtModel,
        array $config = []
    ) {
        parent::__construct($id, $controller, $config);
    }

    /**
     * /user/login
     * @throws DomainException
     */
    public function run(): LoginResponse
    {
        $userEntity = $this->userModel->get($this->userRequest->email, $this->userRequest->password);
        $jwt = $this->jwtModel->get($userEntity->id);

        return new LoginResponse($jwt);
    }
}