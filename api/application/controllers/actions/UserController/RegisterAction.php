<?php

declare(strict_types=1);

namespace api\application\controllers\actions\UserController;

use api\application\controllers\actions\AbstractAction;
use api\application\requests\UserRequest;
use shared\domain\models\UserModel;
use yii\base\Action;
use yii\web\Controller;
use shared\domain\exceptions\DomainException;

class RegisterAction extends AbstractAction
{

    public function __construct(
        string $id,
        Controller $controller,
        private UserRequest $userRequest,
        private UserModel $userModel,
        array $config = []
    ) {
        parent::__construct($id, $controller, $config);
    }

    /**
     * /user/register
     * @throws DomainException
     */
    public function run(): void
    {
        $this->userModel->create($this->userRequest->email, $this->userRequest->password);
    }
}