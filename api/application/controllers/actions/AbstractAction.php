<?php

declare(strict_types=1);

namespace api\application\controllers\actions;

use yii\base\Action;
use yii\web\Controller;

class AbstractAction extends Action
{
    public function __construct(string $id, Controller $controller, array $config = [])
    {
        parent::__construct($id, $controller, $config);
    }
}