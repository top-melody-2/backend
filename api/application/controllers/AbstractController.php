<?php

declare(strict_types=1);

namespace api\application\controllers;

use api\domain\dto\BaseResponseDTO;
use api\domain\exceptions\ValidationException;
use Throwable;
use yii\base\Action;
use yii\base\Module;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;

abstract class AbstractController extends Controller
{

    private const STATUS_CODE_VALIDATION_ERRORS = 422;
    private const STATUS_CODE_DEFAULT = 500;

    public function __construct(string $id, Module $module)
    {
        parent::__construct($id, $module);
    }

    final public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => $this->getVerbsActions(),
            ],
        ];
    }

    /**
     * Метод, ограничивающий доступные HTTP-методы
     * @return array<string, string[]> ['action-in-kebab-kase' => ['GET', 'POST']]
     */
    abstract protected function getVerbsActions(): array;

    final public function isEnableCsrfValidation(): bool
    {
        return false;
    }

    /**
     * @param Action $action
     * @throws InvalidConfigException
     * @throws \yii\web\BadRequestHttpException
     */
    final public function beforeAction($action): bool
    {
        if ($this->request->isOptions) {
            return false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @param string $id
     * @param mixed[] $params
     * @throws InvalidConfigException
     */
    final public function runAction($id, $params = []): BaseResponseDTO
    {
        $dto = new BaseResponseDTO();

        try {
            $dto->response = parent::runAction($id, $params);
        } catch (ValidationException $exception) {
            $dto->success = false;
            $dto->validationErrors = $exception->errors;

            $this->response->statusCode = self::STATUS_CODE_VALIDATION_ERRORS;
        } catch (Throwable $exception) {
            $dto->success = false;
            $dto->error = $exception->getMessage();

            if (!YII_ENV_PROD) {
                $dto->file = $exception->getFile();
                $dto->line = $exception->getLine();
                $dto->trace = $exception->getTraceAsString();
            }

            $this->response->statusCode = $exception->getCode() ?: self::STATUS_CODE_DEFAULT;
        }

        return $dto;
    }
}