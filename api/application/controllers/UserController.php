<?php

declare(strict_types=1);

namespace api\application\controllers;

use api\application\controllers\actions\UserController\LoginAction;
use api\application\controllers\actions\UserController\LogoutAction;
use api\application\controllers\actions\UserController\RegisterAction;

class UserController extends AbstractController
{

    /**
     * @inheritDoc
     */
    protected function getVerbsActions(): array
    {
        return [
            'register' => ['POST'],
            'login' => ['POST'],
            'logout' => ['POST'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'register' => RegisterAction::class,
            'login' => LoginAction::class,
            'logout' => LogoutAction::class,
        ];
    }
}