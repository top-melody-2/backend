<?php

declare(strict_types=1);

namespace api\application\controllers;

use api\domain\dto\SiteIndexDTO;
use api\domain\services\DtoBuilder;
use yii\base\Module;

class SiteController extends AbstractController
{
    public function __construct(string $id, Module $module, private DtoBuilder $builder)
    {
        parent::__construct($id, $module);
    }

    /**
     * @return array<string, string[]>
     */
    protected function getVerbsActions(): array
    {
        return [
            'index' => ['GET'],
        ];
    }

    public function actionIndex(): SiteIndexDTO
    {
        return $this->builder->getSiteIndexDTO();
    }
}