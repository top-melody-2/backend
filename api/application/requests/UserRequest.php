<?php

declare(strict_types=1);

namespace api\application\requests;

class UserRequest extends AbstractRequest
{

    public $email;
    public $password;

    public function rules(): array
    {
        return [
            [['email', 'password'], 'string'],
            [['email', 'password'], 'required'],
            ['email', 'email'],
        ];
    }
}