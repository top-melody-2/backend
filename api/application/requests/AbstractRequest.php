<?php

declare(strict_types=1);

namespace api\application\requests;

use api\domain\exceptions\ValidationException;
use shared\application\exceptions\ApplicationException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use Yii;
use yii\web\Request;

class AbstractRequest extends Model
{

    /**
     * @throws ValidationException
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        $params = Yii::$app->request->getQueryParams() + Yii::$app->request->getBodyParams();
        if (!$this->load($params, '')) {
            throw new ApplicationException('Ошибка загрузки входных данных');
        }

        if (!$this->validate()) {
            throw new ValidationException($this->getErrors());
        }
    }
}