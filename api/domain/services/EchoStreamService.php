<?php

declare(strict_types=1);

namespace api\domain\services;

use backendIntegrationCore\domain\dto\StreamInterface;
use yii\helpers\FileHelper;
use yii\web\Response;

class EchoStreamService
{
    public function __construct(private Response $response) {}

    public function service(StreamInterface $stream): void
    {
        $fileName = implode('.', [$stream->getName(), $stream->getExtension()]);
        $mimeType = FileHelper::getMimeTypeByExtension($stream->getExtension());
        $this->response->setDownloadHeaders($fileName, $mimeType);
        $this->response->headers->add('Access-Control-Allow-Origin', '*');
        $this->response->headers->add('Access-Control-Allow-Methods', implode(',', ['GET', 'OPTIONS']));
        $this->response->headers->add('Accept-Ranges', 'bytes');
        $this->response->headers->add('Content-Length', $stream->getSizeInBytes());

        $this->response->stream = $stream->getStream();
        $this->response->send();
    }
}