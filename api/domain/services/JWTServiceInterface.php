<?php

declare(strict_types=1);

namespace api\domain\services;

use DateTimeImmutable;
use DateTimeInterface;
use shared\domain\exceptions\DomainException;

interface JWTServiceInterface
{

    /**
     * @throws DomainException
     */
    public function get(int $userId): string;

    /**
     * @throws DomainException
     */
    public function isExpired(string $jwt, DateTimeInterface $toDateTime = new DateTimeImmutable()): bool;

    /**
     * @throws DomainException
     */
    public function getUserId(string $jwt): ?int;
}