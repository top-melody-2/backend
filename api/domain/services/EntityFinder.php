<?php

declare(strict_types=1);

namespace api\domain\services;

use shared\domain\entity\PlaylistEntity;
use shared\domain\entity\TrackEntity;
use shared\domain\exceptions\DomainException;
use shared\domain\repository\PlaylistsRepositoryInterface;
use shared\domain\repository\TracksRepositoryInterface;

class EntityFinder
{

    public function __construct(
        private PlaylistsRepositoryInterface $playlistsRepository,
        private TracksRepositoryInterface $tracksRepository,
    ) {}

    /**
     * @return PlaylistEntity[]
     */
    public function getAllPlaylists(): array
    {
        return $this->playlistsRepository->getAll();
    }

    /**
     * @throws DomainException
     */
    public function getPlaylist(int $id): PlaylistEntity
    {
        $playlistEntity = $this->playlistsRepository->getById($id);

        if (!$playlistEntity) {
            $template = 'Плейлист с id %s не найден';
            $message = sprintf($template, $id);
            throw new DomainException($message);
        }

        return $playlistEntity;
    }

    /**
     * @throws DomainException
     */
    public function getTrack(int $id): TrackEntity
    {
        $trackEntity = $this->tracksRepository->getById($id);

        if (!$trackEntity) {
            $template = 'Трек с id %s не найден';
            $message = sprintf($template, $id);
            throw new DomainException($message);
        }

        return $trackEntity;
    }
}