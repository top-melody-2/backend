<?php

declare(strict_types=1);

namespace api\domain\services;

interface RequestServiceInterface
{

    public function getAuthHeader(): ?string;
}