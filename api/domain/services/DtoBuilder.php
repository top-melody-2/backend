<?php

declare(strict_types=1);

namespace api\domain\services;

use api\domain\dto\SiteIndexDTO;
use api\domain\dto\PlaylistDTO;
use api\domain\dto\TrackDTO;
use integrations\domain\models\PlaylistsModel;
use integrations\domain\models\TracksModel;
use shared\domain\entity\PlaylistEntity;
use shared\domain\entity\TrackEntity;

class DtoBuilder
{
    public function __construct(
        private PlaylistsModel $playlistsModel,
        private TracksModel $tracksModel,
    ) {}

    public function getSiteIndexDTO(): SiteIndexDTO
    {
        return new SiteIndexDTO(
            'Приветствую в API для прослушивания музыки Top Melody!',
            date('Y-m-d H:i:s')
        );
    }

    public function getPlaylistDTO(PlaylistEntity $playlistEntity): PlaylistDTO
    {
        return new PlaylistDTO(
            $playlistEntity->id,
            $this->playlistsModel->getDisplayName($playlistEntity),
        );
    }

    public function getTrackDTO(TrackEntity $trackEntity): TrackDTO
    {
        return new TrackDTO(
            $trackEntity->id,
            $trackEntity->name,
            $trackEntity->duration,
            $this->tracksModel->canLike($trackEntity),
            $this->tracksModel->canDislike($trackEntity),
        );
    }
}