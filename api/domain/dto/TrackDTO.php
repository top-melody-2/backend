<?php

declare(strict_types=1);

namespace api\domain\dto;

class TrackDTO
{
    public function __construct(
        public int $id,
        public string $name,
        public int $duration,
        public bool $canLike,
        public bool $canDislike,
    ) {}
}