<?php

declare(strict_types=1);

namespace api\domain\dto;

class PlaylistDTO
{
    public function __construct(
        public int $id,
        public string $name,
    ) {}
}