<?php

declare(strict_types=1);

namespace api\domain\dto;

class BaseResponseDTO
{

    public bool $success = true;

    public ?string $error = null;

    /**
     * @var array<string, string[]>
     */
    public array $validationErrors = [];

    public ?string $file = null;

    public ?int $line = null;

    public ?string $trace = null;

    public mixed $response = null;
}
