<?php

declare(strict_types=1);

namespace api\domain\dto;

class SiteIndexDTO
{
    public function __construct(
        public string $message,
        public string $dateTime,
    ) {}
}