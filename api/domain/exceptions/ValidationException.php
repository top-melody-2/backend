<?php

declare(strict_types=1);

namespace api\domain\exceptions;

use shared\domain\exceptions\DomainException;

class ValidationException extends DomainException
{

    /**
     * @param array<string, string[]> $errors
     */
    public function __construct(public array $errors)
    {
        parent::__construct('', 0, null);
    }
}