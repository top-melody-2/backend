<?php

declare(strict_types=1);

namespace api\domain\exceptions;

use shared\domain\exceptions\DomainException;

class UserException extends DomainException
{

    public function __construct(string $message = "")
    {
        parent::__construct($message, 403);
    }
}