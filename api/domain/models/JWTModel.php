<?php

declare(strict_types=1);

namespace api\domain\models;

use api\domain\exceptions\UserException;
use api\domain\services\JWTServiceInterface;
use api\domain\services\RequestServiceInterface;
use shared\domain\entity\RecalledJWT;
use shared\domain\entity\UserEntity;
use shared\domain\exceptions\DomainException;
use shared\domain\repository\JWTRepositoryInterface;
use shared\domain\repository\UserRepositoryInterface;

class JWTModel
{

    public function __construct(
        private JWTServiceInterface     $jwtService,
        private JWTRepositoryInterface  $jwtRepository,
        private RequestServiceInterface $requestService,
        private UserRepositoryInterface $userRepository,
    ) {}

    /**
     * @throws DomainException
     */
    public function get(int $userId): string
    {
        return $this->jwtService->get($userId);
    }

    /**
     * @throws DomainException
     */
    public function recall(string $jwt): void
    {
        $recalledJWT = new RecalledJWT();
        $recalledJWT->jwt = $jwt;

        $this->jwtRepository->save($recalledJWT);
    }

    /**
     * @throws UserException
     */
    public function getCurrent(): string
    {
        $authHeader = $this->requestService->getAuthHeader();
        if (!$authHeader) {
            throw new UserException('Не найден заголовок аутентификации');
        }

        preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches);
        $jwt = $matches[1] ?? null;
        if (!$jwt) {
            throw new UserException('Не найден токен аутентификации');
        }

        if ($this->jwtService->isExpired($jwt)) {
            throw new UserException('Токен аутентификации просрочен');
        }

        if ($this->jwtRepository->isRecalled($jwt)) {
            throw new UserException('Токен аутентификации отозван. Необходимо залогиниться снова');
        }

        return $jwt;
    }

    /**
     * @throws UserException
     */
    public function getUser(): UserEntity
    {
        $jwt = $this->getCurrent();
        $userId = $this->jwtService->getUserId($jwt);
        if (!$userId) {
            throw new UserException('В токене не найден id пользователя');
        }

        $userEntity = $this->userRepository->getById($userId);
        if (!$userEntity) {
            throw new UserException('Пользователь не найден по токену аутентификации');
        }

        return $userEntity;
    }
}