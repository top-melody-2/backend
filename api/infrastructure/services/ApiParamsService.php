<?php

declare(strict_types=1);

namespace api\infrastructure\services;

use shared\domain\exceptions\DomainException;
use shared\infrastructure\services\traits\GetParamsValueTrait;
use Yii;

class ApiParamsService
{

    use GetParamsValueTrait;

    /**
     * @throws DomainException
     */
    public function getApiUrl(): string
    {
        return (string) $this->getParamsValue('apiUrl');
    }

    /**
     * @throws DomainException
     */
    public function getJwtTtl(): int
    {
        return (int) $this->getParamsValue('jwtTtl');
    }

    /**
     * @throws DomainException
     */
    public function getJtiClaim(): string
    {
        return (string) $this->getParamsValue('jtiClaim');
    }
}