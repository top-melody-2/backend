<?php

declare(strict_types=1);

namespace api\infrastructure\services;

use api\domain\services\RequestServiceInterface;
use Yii;

class RequestService implements RequestServiceInterface
{

    public function getAuthHeader(): ?string
    {
        return Yii::$app->request->getHeaders()->get('Authorization');
    }
}