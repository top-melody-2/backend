<?php

declare(strict_types=1);

namespace api\infrastructure\services;

use DateTimeImmutable;
use DateTimeInterface;
use api\domain\services\JWTServiceInterface;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\JwtFacade;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Validation\Constraint;
use shared\domain\exceptions\DomainException;
use shared\infrastructure\components\JwtSystemClock;
use Throwable;

class JWTService implements JWTServiceInterface
{

    private const CLAIM_USER_ID = 'uid';

    public function __construct(
        private JwtFacade        $jwtFacade,
        private Sha256           $sha256,
        private JwtSystemClock   $jwtSystemClock,
        private ApiParamsService $apiParamsService,
    ) {}

    /**
     * @inheritDoc
     */
    public function get(int $userId): string
    {
        try {
            $token = $this->jwtFacade->issue(
                $this->sha256,
                $this->getJtiClaim(),
                fn (Builder $builder, DateTimeImmutable $issuedAt): Builder => $builder
                    ->issuedBy($this->apiParamsService->getApiUrl())
                    ->permittedFor($this->apiParamsService->getApiUrl())
                    ->expiresAt($issuedAt->modify('+' . $this->apiParamsService->getJwtTtl() . ' seconds'))
                    ->withClaim(self::CLAIM_USER_ID, $userId)
            );

            return $token->toString();
        } catch (Throwable $exception) {
            throw new DomainException('Не удалось создать JWT: ' . $exception->getMessage());
        }
    }

    private function getJtiClaim(): InMemory
    {
        return InMemory::base64Encoded($this->apiParamsService->getJtiClaim());
    }

    /**
     * @throws DomainException
     */
    public function isExpired(string $jwt, DateTimeInterface $toDateTime = new DateTimeImmutable()): bool
    {
        $token = $this->getFromString($jwt);
        return $token->isExpired(new DateTimeImmutable());
    }

    /**
     * @throws DomainException
     */
    private function getFromString(string $jwt): Token
    {
        try {
            return $this->jwtFacade->parse(
                $jwt,
                new Constraint\SignedWith($this->sha256, $this->getJtiClaim()),
                new Constraint\StrictValidAt($this->jwtSystemClock)
            );
        } catch (Throwable $exception) {
            throw new DomainException('Токен аутентификации не валиден');
        }
    }

    /**
     * @throws DomainException
     */
    public function getUserId(string $jwt): ?int
    {
        $token = $this->getFromString($jwt);
        return $token->claims()->get(self::CLAIM_USER_ID);
    }
}