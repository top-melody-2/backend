<?php

use api\domain\services\JWTServiceInterface;
use api\domain\services\RequestServiceInterface;
use api\infrastructure\services\JWTService;
use api\infrastructure\services\RequestService;
use Lcobucci\JWT\Decoder;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Parser;

$params = array_merge(
    require __DIR__ . '/../../shared/config/params.php',
    require __DIR__ . '/../../shared/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api-v1',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\application\controllers',
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => yii\web\JsonParser::class,
            ],
        ],
        'response' => [
            'format' => \yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => yii\web\JsonResponseFormatter::class,
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
            'on beforeSend' => function($event) {
                $event->sender->headers->add('Access-Control-Allow-Origin', '*');
                $event->sender->headers->add('Access-Control-Request-Method', 'GET,HEAD,POST,PUT,PATCH,DELETE,OPTIONS');
                $event->sender->headers->add('Access-Control-Request-Headers', '*');
                $event->sender->headers->add('Access-Control-Allow-Credentials', 'true');
                $event->sender->headers->add('Access-Control-Max-Age', 86400);
                $event->sender->headers->add('Access-Control-Expose-Headers', '');
                $event->sender->headers->add('Access-Control-Allow-Headers', 'Content-Type,Authorization');
            },
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'playlists/<playlist:\d+>/tracks/next' => 'playlists/next-track',
                'playlists/<playlist:\d+>/tracks/prev' => 'playlists/prev-track',
                'tracks/<track:\d+>/stream' => 'tracks/stream',
                'tracks/<track:\d+>/like' => 'tracks/like',
                'tracks/<track:\d+>/dislike' => 'tracks/dislike',
            ],
        ],
    ],
    'params' => $params,
    'container' => [
        'definitions' => [
            JWTServiceInterface::class => JWTService::class,
            RequestServiceInterface::class => RequestService::class,
            Decoder::class => JoseEncoder::class,
            Parser::class => \Lcobucci\JWT\Token\Parser::class,
        ]
    ]
];
