<?php

return [
    'Development' => [
        'path' => 'dev',
        'setWritable' => [
            'api/runtime',
            'console/runtime',
            'docker/containers/mysql/data',
        ],
        'setExecutable' => [
            'yii',
        ],
    ],
    'Production' => [
        'path' => 'prod',
        'setWritable' => [
            'api/runtime',
            'console/runtime',
            'docker/containers/mysql/data',
        ],
        'setExecutable' => [
            'yii',
        ],
    ],
];
