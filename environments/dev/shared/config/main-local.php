<?php

return [
    'components' => [
        'db' => [
            'class' => \yii\db\Connection::class,
            'dsn' => 'mysql:host=mysql;dbname=tm2',
            'username' => 'tm2',
            'password' => 'tm2',
            'charset' => 'utf8',
        ],
    ],
];
