<?php

declare(strict_types=1);

namespace integrations\domain\models;

use backendIntegrationCore\domain\dto\StreamInterface;
use integrations\domain\services\EntityFiller;
use integrations\domain\services\IntegrationFactory;
use integrations\domain\services\RequestDTOBuilder;
use shared\domain\entity\PlaylistEntity;
use shared\domain\entity\TrackEntity;
use shared\domain\entity\TrackIntegrationConfigEntity;
use shared\domain\repository\IntegrationsRepositoryInterface;
use shared\domain\repository\PlaylistsRepositoryInterface;
use shared\domain\repository\TracksRepositoryInterface;
use Throwable;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use shared\domain\exceptions\DomainException;

class TracksModel
{

    public function __construct(
        private IntegrationFactory              $integrationFactory,
        private IntegrationsRepositoryInterface $integrationsRepository,
        private PlaylistsRepositoryInterface    $playlistsRepository,
        private TracksRepositoryInterface       $tracksRepository,
        private RequestDTOBuilder               $builder,
        private PlaylistsModel                  $playlistsModel,
    ) {}

    /**
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function getStream(TrackEntity $trackEntity): StreamInterface
    {
        $integrationEntity = array_values($trackEntity->integrations)[0];
        $trackConfig = $this->tracksRepository->getConfig($trackEntity->id, $integrationEntity->id);
        if (!$trackConfig) {
            $messageTemplate = "Не найдена конфигурация трека %s для интеграции %s";
            $message = sprintf($messageTemplate, $trackEntity->name, $integrationEntity->name);
            throw new DomainException($message);
        }

        $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
        $trackDTO = $this->builder->getTrackDTO($trackEntity, $trackConfig);

        $response = $this->integrationFactory
            ->service($integrationEntity->class)
            ->getTrackStream($integrationDTO, $trackDTO);
        return $response->stream;
    }

    /**
     * Проверяет, можно ли лайкнуть трек хотя бы в одном плейлисте всех интеграций, связанных с треком
     * @throws Throwable
     * @throws IntegrationException
     * @throws DomainException
     */
    public function canLike(TrackEntity $trackEntity): bool
    {
        foreach ($trackEntity->integrations as $integrationEntity) {
            $trackConfigEntity = $this->tracksRepository->getConfig($trackEntity->id, $integrationEntity->id);
            if (!$trackConfigEntity) {
                $messageTemplate = "Не найдена конфигурация трека %s для интеграции %s";
                $message = sprintf($messageTemplate, $trackEntity->name, $integrationEntity->name);
                throw new DomainException($message);
            }

            $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
            $trackDTO = $this->builder->getTrackDTO($trackEntity, $trackConfigEntity);

            foreach ($integrationEntity->playlists as $playlistEntity) {
                $playlistDTO = $this->builder->getPlaylistDTO($playlistEntity);

                $canLikeInCurrentPlaylist = $this->integrationFactory
                    ->service($integrationEntity->class)
                    ->canLikeTrack($integrationDTO, $playlistDTO, $trackDTO);

                if ($canLikeInCurrentPlaylist) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Лайкает трек во всех плейлистах привязанных к треку интеграций
     * @return string Сообщение о том, в какие плейлисты трек был добавлен
     * @throws DomainException
     * @throws IntegrationException
     * @throws Throwable
     * @throws NeedLogException
     */
    public function like(TrackEntity $trackEntity): string
    {
        if (!$this->canLike($trackEntity)) {
            $messageTemplate = "Нельзя лайкнуть трек %s ни в одном из плейлистов";
            $message = sprintf($messageTemplate, $trackEntity->name);
            throw new DomainException($message);
        }

        $likedInPlaylistEntities = [];
        foreach ($trackEntity->integrations as $integrationEntity) {
            $trackConfigEntity = $this->tracksRepository->getConfig($trackEntity->id, $integrationEntity->id);
            if (!$trackConfigEntity) {
                $messageTemplate = "Не найдена конфигурация трека %s для интеграции %s";
                $message = sprintf($messageTemplate, $trackEntity->name, $integrationEntity->name);
                throw new DomainException($message);
            }

            $integration = $this->integrationFactory->service($integrationEntity->class);
            $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
            $trackDTO = $this->builder->getTrackDTO($trackEntity, $trackConfigEntity);

            foreach ($integrationEntity->playlists as $playlistEntity) {
                $playlistDTO = $this->builder->getPlaylistDTO($playlistEntity);

                $canLikeInCurrentPlaylist = $integration->canLikeTrack($integrationDTO, $playlistDTO, $trackDTO);
                if (!$canLikeInCurrentPlaylist) {
                    continue;
                }

                $response = $integration->likeTrack($integrationDTO, $playlistDTO, $trackDTO);
                $this->integrationsRepository->updateConfig($integrationEntity->id, $response->integrationConfig->toArray());
                $this->playlistsRepository->updateConfig($playlistEntity->id, $response->playlistConfig->toArray());
                $this->tracksRepository->updateConfig($trackEntity->id, $integrationEntity->id, $response->trackConfig->toArray());
                $this->tracksRepository->linkToPlaylist($trackEntity->id, $playlistEntity->id);
                $likedInPlaylistEntities[] = $playlistEntity;
            }
        }

        $template = 'Трек %s был добавлен в следующие плейлисты: %s';
        $playlistNames = array_map(
            fn (PlaylistEntity $playlistEntity) => $this->playlistsModel->getDisplayName($playlistEntity),
            $likedInPlaylistEntities
        );
        return sprintf($template, $trackEntity->name, implode(', ', $playlistNames));
    }

    /**
     * Проверяет, можно ли дизлайкнуть трек хотя бы в одном плейлисте всех интеграций, связанных с треком
     * @throws Throwable
     * @throws IntegrationException
     * @throws DomainException
     */
    public function canDislike(TrackEntity $trackEntity): bool
    {
        foreach ($trackEntity->integrations as $integrationEntity) {
            $trackConfigEntity = $this->tracksRepository->getConfig($trackEntity->id, $integrationEntity->id);
            if (!$trackConfigEntity) {
                $messageTemplate = "Не найдена конфигурация трека %s для интеграции %s";
                $message = sprintf($messageTemplate, $trackEntity->name, $integrationEntity->name);
                throw new DomainException($message);
            }

            $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
            $trackDTO = $this->builder->getTrackDTO($trackEntity, $trackConfigEntity);

            foreach ($integrationEntity->playlists as $playlistEntity) {
                $playlistDTO = $this->builder->getPlaylistDTO($playlistEntity);

                $canDislikeInCurrentPlaylist = $this->integrationFactory
                    ->service($integrationEntity->class)
                    ->canDislikeTrack($integrationDTO, $playlistDTO, $trackDTO);

                if ($canDislikeInCurrentPlaylist) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Дизлайкает трек во всех плейлистах привязанных к треку интеграций
     * @return string Сообщение о том, из каких плейлистов трек был удален
     * @throws DomainException
     * @throws IntegrationException
     * @throws Throwable
     * @throws NeedLogException
     */
    public function dislike(TrackEntity $trackEntity): string
    {
        if (!$this->canDislike($trackEntity)) {
            $messageTemplate = "Нельзя дизлайкнуть трек %s ни в одном из плейлистов";
            $message = sprintf($messageTemplate, $trackEntity->name);
            throw new DomainException($message);
        }

        $dislikedInPlaylistEntities = [];
        foreach ($trackEntity->integrations as $integrationEntity) {
            $trackConfigEntity = $this->tracksRepository->getConfig($trackEntity->id, $integrationEntity->id);
            if (!$trackConfigEntity) {
                $messageTemplate = "Не найдена конфигурация трека %s для интеграции %s";
                $message = sprintf($messageTemplate, $trackEntity->name, $integrationEntity->name);
                throw new DomainException($message);
            }

            $integration = $this->integrationFactory->service($integrationEntity->class);
            $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
            $trackDTO = $this->builder->getTrackDTO($trackEntity, $trackConfigEntity);

            foreach ($integrationEntity->playlists as $playlistEntity) {
                $playlistDTO = $this->builder->getPlaylistDTO($playlistEntity);

                $canDislikeInCurrentPlaylist = $integration->canDislikeTrack($integrationDTO, $playlistDTO, $trackDTO);
                if (!$canDislikeInCurrentPlaylist) {
                    continue;
                }

                $response = $integration->dislikeTrack($integrationDTO, $playlistDTO, $trackDTO);
                $this->integrationsRepository->updateConfig($integrationEntity->id, $response->integrationConfig->toArray());
                $this->playlistsRepository->updateConfig($playlistEntity->id, $response->playlistConfig->toArray());
                $this->tracksRepository->updateConfig($trackEntity->id, $integrationEntity->id, $response->trackConfig->toArray());
                $this->tracksRepository->unlinkFromPlaylist($trackEntity->id, $playlistEntity->id);
                $dislikedInPlaylistEntities[] = $playlistEntity;
            }
        }

        $template = 'Трек %s был удален из следующих плейлистов: %s';
        $playlistNames = array_map(
            fn (PlaylistEntity $playlistEntity) => $this->playlistsModel->getDisplayName($playlistEntity),
            $dislikedInPlaylistEntities
        );
        return sprintf($template, $trackEntity->name, implode(', ', $playlistNames));
    }
}