<?php

declare(strict_types=1);

namespace integrations\domain\models;

use backendVkIntegration\domain\VkIntegration;
use integrations\domain\IntegrationsCollection;
use integrations\domain\services\EntityFiller;
use integrations\domain\services\IntegrationFactory;
use shared\domain\entity\IntegrationEntity;
use shared\domain\repository\IntegrationsRepositoryInterface;
use Throwable;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use shared\domain\exceptions\DomainException;

class IntegrationsModel
{

    public function __construct(
        private IntegrationsRepositoryInterface $integrationsRepository,
        private IntegrationFactory $integrationFactory,
        private EntityFiller $filler,
        private IntegrationsCollection $integrationsCollection,
    ) {}

    /**
     * @throws Throwable
     * @throws IntegrationException
     * @throws DomainException
     */
    public function updateList(): void
    {
        $existIntegrationClasses = $this->integrationsRepository->getAllClasses();

        foreach ($this->integrationsCollection->getAllClasses() as $class) {
            if (in_array($class, $existIntegrationClasses)) {
                continue;
            }

            $response = $this->integrationFactory->service($class)->getInfo();
            $integrationEntity = $this->filler->fillIntegration(new IntegrationEntity(), $response->integrationDTO);
            $integrationEntity->class = $class;
            $this->integrationsRepository->save($integrationEntity);
        }
    }
}