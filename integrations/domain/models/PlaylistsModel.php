<?php

declare(strict_types=1);

namespace integrations\domain\models;

use integrations\domain\services\EntityFiller;
use integrations\domain\services\IntegrationFactory;
use integrations\domain\services\RequestDTOBuilder;
use shared\domain\entity\PlaylistEntity;
use shared\domain\entity\TrackEntity;
use shared\domain\entity\TrackIntegrationConfigEntity;
use shared\domain\exceptions\DomainException;
use shared\domain\repository\IntegrationsRepositoryInterface;
use shared\domain\repository\PlaylistsRepositoryInterface;
use shared\domain\repository\TracksRepositoryInterface;
use Throwable;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use backendIntegrationCore\domain\exceptions\NeedLogException;

class PlaylistsModel
{

    public function __construct(
        private IntegrationFactory              $integrationFactory,
        private IntegrationsRepositoryInterface $integrationsRepository,
        private PlaylistsRepositoryInterface    $playlistsRepository,
        private TracksRepositoryInterface       $tracksRepository,
        private RequestDTOBuilder               $builder,
        private EntityFiller                    $filler,
    ) {}

    /**
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function updateList(): void
    {
        foreach ($this->integrationsRepository->getAll() as $integrationEntity) {
            $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
            $response = $this->integrationFactory->service($integrationEntity->class)->getPlaylists($integrationDTO);
            $this->integrationsRepository->updateConfig($integrationEntity->id, $response->integrationConfig->toArray());

            $existPlaylistNames = array_map(
                fn (PlaylistEntity $playlistEntity) => $playlistEntity->name,
                $this->playlistsRepository->getByIntegration($integrationEntity->id)
            );

            foreach ($response->playlistDtoList as $playlistDTO) {
                if (in_array($playlistDTO->name, $existPlaylistNames)) {
                    continue;
                }

                $playlistEntity = $this->filler->fillPlaylist(new PlaylistEntity(), $playlistDTO);
                $playlistEntity->integration_id = $integrationEntity->id;
                $this->playlistsRepository->save($playlistEntity);
            }
        }
    }

    /**
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function updateConfigs(): void
    {
        foreach ($this->integrationsRepository->getAll() as $integrationEntity) {
            $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
            $response = $this->integrationFactory->service($integrationEntity->class)->getPlaylists($integrationDTO);
            $this->integrationsRepository->updateConfig($integrationEntity->id, $response->integrationConfig->toArray());

            foreach ($response->playlistDtoList as $playlistDTO) {
                $playlistEntity = $this->playlistsRepository
                    ->getByIntegrationAndName($integrationEntity->id, $playlistDTO->name);
                if (!$playlistEntity) {
                    $messageTemplate = 'Плейлист %s интеграции %s не найден в базе данных';
                    $message = sprintf($messageTemplate, $playlistDTO->name, $integrationEntity->name);
                    throw new DomainException($message);
                }

                $this->playlistsRepository->updateConfig($playlistEntity->id, $playlistDTO->config->toArray());
            }
        }
    }

    /**
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function getNextTrack(PlaylistEntity $playlistEntity): TrackEntity
    {
        $integrationEntity = $playlistEntity->integration;

        $integrationDTO = $this->builder->getIntegrationDTO($integrationEntity);
        $playlistDTO = $this->builder->getPlaylistDTO($playlistEntity);

        $response = $this->integrationFactory
            ->service($integrationEntity->class)
            ->getNextTrack($integrationDTO, $playlistDTO);

        $this->integrationsRepository->updateConfig($integrationEntity->id, $response->integrationConfig->toArray());
        $this->playlistsRepository->updateConfig($playlistEntity->id, $response->playlistConfig->toArray());

        $trackEntity = $this->tracksRepository->getByName($response->trackDTO->name);
        if (!$trackEntity) {
            $trackEntity = $this->filler->fillTrack(new TrackEntity(), $response->trackDTO);
            $this->tracksRepository->save($trackEntity);
        }

        $trackConfigEntity = $this->tracksRepository->getConfig($trackEntity->id, $integrationEntity->id)
            ?? new TrackIntegrationConfigEntity();
        $trackConfigEntity = $this->filler->fillTrackConfig($trackConfigEntity, $response->trackDTO);
        $trackConfigEntity->integration_id = $integrationEntity->id;
        $trackConfigEntity->track_id = $trackEntity->id;
        $this->tracksRepository->save($trackConfigEntity);

        if (!$this->tracksRepository->isInIntegration($trackEntity->id, $integrationEntity->id)) {
            $this->tracksRepository->linkToIntegration($trackEntity->id, $integrationEntity->id);
        }

        if (!$this->tracksRepository->isInPlaylist($trackEntity->id, $integrationEntity->id)) {
            $this->tracksRepository->linkToPlaylist($trackEntity->id, $playlistEntity->id);
        }

        return $trackEntity;
    }

    public function getDisplayName(PlaylistEntity $playlistEntity): string
    {
        $template = '%s: %s';
        return sprintf($template, $playlistEntity->integration->name, $playlistEntity->name);
    }
}