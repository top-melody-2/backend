<?php

declare(strict_types=1);

namespace integrations\domain\services;

use backendIntegrationCore\domain\IntegrationInterface;
use shared\domain\exceptions\DomainException;
use shared\domain\repository\IntegrationsRepositoryInterface;
use Yii;

class IntegrationFactory
{

    public function __construct(
        private EntityFiller $entityFiller,
        private IntegrationsRepositoryInterface $integrationsRepository,
    ) {}

    /**
     * @param class-string $class
     * @throws DomainException
     */
    public function service(string $class): IntegrationInterface
    {
        if (!class_exists($class) || !in_array(IntegrationInterface::class, class_implements($class))) {
            throw new DomainException("Класс $class не существует или не соответствует интерфейсу интеграции");
        }

        /** @var IntegrationInterface $integration */
        $integration = Yii::createObject($class);
        return new IntegrationRequestLogDecorator(
            $integration,
            $this->entityFiller,
            $this->integrationsRepository
        );
    }
}