<?php

declare(strict_types=1);

namespace integrations\domain\services;

use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\RequestLogDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use shared\domain\entity\IntegrationEntity;
use shared\domain\entity\PlaylistEntity;
use shared\domain\entity\RequestLogEntity;
use shared\domain\entity\TrackEntity;
use shared\domain\entity\TrackIntegrationConfigEntity;
use shared\domain\entity\TrackToIntegrationEntity;
use shared\domain\entity\TrackToPlaylistEntity;

class EntityFiller
{
    public function fillIntegration(IntegrationEntity $entity, IntegrationDTO $dto): IntegrationEntity
    {
        $entity->name = $dto->name;
        $entity->config = $dto->config->toArray();

        return $entity;
    }

    public function fillRequestLog(RequestLogEntity $entity, RequestLogDTO $dto): RequestLogEntity
    {
        $entity->request_url = $dto->requestUrl;
        $entity->response_status_code = $dto->responseStatusCode;
        $entity->request = $dto->request;
        $entity->response = $dto->response;
        $entity->request_headers = $dto->requestHeaders->toArray();
        $entity->response_headers = $dto->responseHeaders->toArray();
        $entity->execution_start_date_time = $dto->executionStartDateTime->format('Y-m-d H:i:s');
        $entity->execution_end_date_time = $dto->executionEndDateTime->format('Y-m-d H:i:s');

        return $entity;
    }

    public function fillPlaylist(PlaylistEntity $entity, PlaylistDTO $dto): PlaylistEntity
    {
        $entity->name = $dto->name;
        $entity->config = $dto->config->toArray();

        return $entity;
    }

    public function fillTrack(TrackEntity $entity, TrackDTO $dto): TrackEntity
    {
        $entity->name = $dto->name;
        $entity->duration = $dto->duration;

        return $entity;
    }

    public function fillTrackConfig(TrackIntegrationConfigEntity $entity, TrackDTO $dto): TrackIntegrationConfigEntity
    {
        $entity->config = $dto->config->toArray();

        return $entity;
    }
}