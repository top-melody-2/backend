<?php

declare(strict_types=1);

namespace integrations\domain\services;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use shared\domain\entity\IntegrationEntity;
use shared\domain\entity\PlaylistEntity;
use shared\domain\entity\TrackEntity;
use shared\domain\entity\TrackIntegrationConfigEntity;

class RequestDTOBuilder
{
    public function getIntegrationDTO(IntegrationEntity $integrationEntity): IntegrationDTO
    {
        return new IntegrationDTO(
            $integrationEntity->name,
            new ConfigCollection($integrationEntity->config ?? []),
        );
    }

    public function getPlaylistDTO(PlaylistEntity $playlistEntity): PlaylistDTO
    {
        return new PlaylistDTO(
            $playlistEntity->name,
            new ConfigCollection($playlistEntity->config ?? []),
        );
    }

    public function getTrackDTO(TrackEntity $trackEntity, TrackIntegrationConfigEntity $trackIntegrationConfigEntity): TrackDTO
    {
        return new TrackDTO(
            $trackEntity->name,
            $trackEntity->duration,
            new ConfigCollection($trackIntegrationConfigEntity->config),
        );
    }
}