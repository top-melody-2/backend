<?php

declare(strict_types=1);

namespace integrations\domain\services;

use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\RequestLogDTO;
use backendIntegrationCore\domain\dto\responses\ChangeTrackStateResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetInfoResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetPlaylistsResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackStreamResponseDTO;
use backendIntegrationCore\domain\dto\responses\LoggableResponseDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use backendIntegrationCore\domain\IntegrationInterface;
use shared\domain\entity\RequestLogEntity;
use shared\domain\exceptions\DomainException;
use shared\domain\repository\IntegrationsRepositoryInterface;
use Throwable;

class IntegrationRequestLogDecorator implements IntegrationInterface
{
    public function __construct(
        private IntegrationInterface            $integration,
        private EntityFiller                    $filler,
        private IntegrationsRepositoryInterface $integrationsRepository,
    ) {}

    /**
     * @throws NeedLogException
     * @throws DomainException
     * @throws IntegrationException
     * @throws Throwable
     */
    private function runWithLog(callable $fn): mixed
    {
        $class = $this->integration::class;
        $integrationEntity = $this->integrationsRepository->getByClass($class);
        if (!$integrationEntity) {
            throw new DomainException("Интеграция $class не найдена по классу");
        }

        try {
            $result = $fn();

            if ($result instanceof LoggableResponseDTO) {
                $this->saveRequestLogs($result->requestLogDtoList, $integrationEntity->id);
            }

            return $result;
        } catch (NeedLogException $exception) {
            $this->saveRequestLogs($exception->requestLogDtoList, $integrationEntity->id);
            throw $exception;
        }
    }

    private function saveRequestLogs(array $requestLogDtoList, int $integrationId): void
    {
        foreach ($requestLogDtoList as $requestLogDTO) {
            $entity = $this->filler->fillRequestLog(new RequestLogEntity(), $requestLogDTO);
            $entity->integration_id = $integrationId;

            $this->integrationsRepository->insertRequestLog($entity);
        }
    }

    /**
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getInfo(): GetInfoResponseDTO
    {
        return $this->integration->getInfo();
    }

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getPlaylists(IntegrationDTO $integrationDTO): GetPlaylistsResponseDTO
    {
        return $this->runWithLog(
            fn () => $this->integration->getPlaylists($integrationDTO)
        );
    }

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getNextTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO): GetTrackResponseDTO
    {
        return $this->runWithLog(
            fn () => $this->integration->getNextTrack($integrationDTO, $playlistDTO)
        );
    }

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function getTrackStream(IntegrationDTO $integrationDTO, TrackDTO $trackDTO): GetTrackStreamResponseDTO
    {
        return $this->runWithLog(
            fn () => $this->integration->getTrackStream($integrationDTO, $trackDTO)
        );
    }

    /**
     * @throws IntegrationException
     * @throws Throwable
     */
    public function canLikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): bool
    {
        return $this->integration->canLikeTrack($integrationDTO, $playlistDTO, $trackDTO);
    }

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function likeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): ChangeTrackStateResponseDTO
    {
        return $this->runWithLog(
            fn () => $this->integration->likeTrack($integrationDTO, $playlistDTO, $trackDTO)
        );
    }

    /**
     * @throws IntegrationException
     * @throws Throwable
     */
    public function canDislikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): bool
    {
        return $this->integration->canDislikeTrack($integrationDTO, $playlistDTO, $trackDTO);
    }

    /**
     * @throws NeedLogException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function dislikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): ChangeTrackStateResponseDTO
    {
        return $this->runWithLog(
            fn () => $this->integration->dislikeTrack($integrationDTO, $playlistDTO, $trackDTO)
        );
    }
}