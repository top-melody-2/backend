<?php

declare(strict_types=1);

namespace integrations\domain;

use backendVkIntegration\domain\VkIntegration;

class IntegrationsCollection
{

    /**
     * @return string[]
     */
    public function getAllClasses(): array
    {
        return [
            VkIntegration::class,
        ];
    }
}