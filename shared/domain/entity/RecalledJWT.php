<?php

declare(strict_types=1);

namespace shared\domain\entity;

/**
 * @property int $id
 * @property string $jwt
 * @property-read string $recalled_at
 */
class RecalledJWT extends AbstractEntity
{

    public function init(): void
    {
        if ($this->getIsNewRecord()) {
            $this->recalled_at = date('Y-m-d H:i:s');
        }
    }

    protected function getProperties(): array
    {
        return [
            'id',
            'jwt',
            'recalled_at',
        ];
    }
}