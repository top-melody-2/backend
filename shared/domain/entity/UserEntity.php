<?php

declare(strict_types=1);

namespace shared\domain\entity;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $email
 * @property string $password_hash
 * @property-read string $created_at
 */
class UserEntity extends AbstractEntity
{

    public static function tableName(): string
    {
        return 'user';
    }

    public function init(): void
    {
        if ($this->getIsNewRecord()) {
            $this->created_at = date('Y-m-d H:i:s');
        }
    }

    /**
     * @inheritDoc
     */
    protected function getProperties(): array
    {
        return [
            'id',
            'email',
            'password_hash',
            'created_at',
        ];
    }
}