<?php

declare(strict_types=1);

namespace shared\domain\entity;

/**
 * @property int $track_id
 * @property int $playlist_id
 */
final class TrackToPlaylistEntity extends AbstractEntity
{
    public static function tableName(): string
    {
        return 'track_to_playlist';
    }

    protected function getProperties(): array
    {
        return [
            'track_id',
            'playlist_id'
        ];
    }

    public static function primaryKey(): array
    {
        return ['track_id', 'playlist_id'];
    }
}