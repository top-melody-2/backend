<?php

declare(strict_types=1);

namespace shared\domain\entity;

use yii\db\ActiveQueryInterface;

/**
 * @property int $id
 * @property string $name
 * @property array<string, mixed> $config
 * @property string $integration_id
 * @property-read TrackToPlaylistEntity[] $trackToPlaylists
 * @property-read TrackEntity[] $tracks
 * @property-read IntegrationEntity $integration
 */
final class PlaylistEntity extends AbstractEntity
{
    public static function tableName(): string
    {
        return 'playlist';
    }

    protected function getProperties(): array
    {
        return [
            'id',
            'name',
            'config',
            'integration_id',
        ];
    }

    public function getTrackToPlaylists(): ActiveQueryInterface
    {
        return $this->hasMany(TrackToPlaylistEntity::class, ['playlist_id' => 'id']);
    }

    public function getTracks(): ActiveQueryInterface
    {
        return $this->hasMany(TrackEntity::class, ['id' => 'track_id'])
            ->via('trackToPlaylists');
    }

    public function getIntegration(): ActiveQueryInterface
    {
        return $this->hasOne(IntegrationEntity::class, ['id' => 'integration_id']);
    }
}