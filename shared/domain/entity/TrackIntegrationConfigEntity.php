<?php

declare(strict_types=1);

namespace shared\domain\entity;

use yii\db\ActiveQueryInterface;

/**
 * @property int $track_id
 * @property int $integration_id
 * @property array<string, mixed> $config
 * @property-read TrackEntity $track
 * @property-read IntegrationEntity $integration
 */
class TrackIntegrationConfigEntity extends AbstractEntity
{
    public static function primaryKey(): array
    {
        return ['track_id', 'integration_id'];
    }

    public static function tableName(): string
    {
        return 'track_integration_config';
    }

    /**
     * @return string[]
     */
    protected function getProperties(): array
    {
        return [
            'track_id',
            'integration_id',
            'config',
        ];
    }

    public function getTrack(): ActiveQueryInterface
    {
        return $this->hasOne(TrackEntity::class, ['id' => 'track_id']);
    }

    public function getIntegration(): ActiveQueryInterface
    {
        return $this->hasOne(IntegrationEntity::class, ['id' => 'integration_id']);
    }
}