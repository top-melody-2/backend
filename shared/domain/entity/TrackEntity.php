<?php

declare(strict_types=1);

namespace shared\domain\entity;

use yii\db\ActiveQueryInterface;

/**
 * @property int $id
 * @property string $name
 * @property int $duration
 * @property-read TrackToPlaylistEntity[] $trackToPlaylists
 * @property-read PlaylistEntity[] $playlists
 * @property-read TrackToIntegrationEntity[] $trackToIntegrations
 * @property-read IntegrationEntity[] $integrations
 * @property-read TrackIntegrationConfigEntity[] $configs
 */
final class TrackEntity extends AbstractEntity
{
    public static function tableName(): string
    {
        return 'track';
    }

    protected function getProperties(): array
    {
        return [
            'id',
            'name',
            'duration',
        ];
    }

    public function getTrackToPlaylists(): ActiveQueryInterface
    {
        return $this->hasMany(TrackToPlaylistEntity::class, ['track_id' => 'id']);
    }

    public function getPlaylists(): ActiveQueryInterface
    {
        return $this->hasMany(PlaylistEntity::class, ['id' => 'playlist_id'])
            ->via('trackToPlaylists');
    }

    public function getTrackToIntegrations(): ActiveQueryInterface
    {
        return $this->hasMany(TrackToIntegrationEntity::class, ['track_id' => 'id']);
    }

    public function getIntegrations(): ActiveQueryInterface
    {
        return $this->hasMany(IntegrationEntity::class, ['id' => 'integration_id'])
            ->via('trackToIntegrations');
    }

    public function getConfigs(): ActiveQueryInterface
    {
        return $this->hasMany(TrackIntegrationConfigEntity::class, ['track_id' => 'id']);
    }
}