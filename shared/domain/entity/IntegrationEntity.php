<?php

declare(strict_types=1);

namespace shared\domain\entity;

use yii\db\ActiveQueryInterface;

/**
 * @property int $id
 * @property string $name
 * @property class-string $class
 * @property array<string, mixed> $config
 * @property-read RequestLogEntity[] $requestLogs
 * @property-read PlaylistEntity[] $playlists
 * @property-read TrackToIntegrationEntity[] $trackToIntegrations
 * @property-read TrackEntity[] $tracks
 */
class IntegrationEntity extends AbstractEntity
{
    public static function tableName(): string
    {
        return 'integration';
    }

    /**
     * @return string[]
     */
    protected function getProperties(): array
    {
        return [
            'id',
            'name',
            'class',
            'config',
        ];
    }

    public function getRequestLogs(): ActiveQueryInterface
    {
        return $this->hasMany(RequestLogEntity::class, ['integration_id' => 'id']);
    }

    public function getPlaylists(): ActiveQueryInterface
    {
        return $this->hasMany(PlaylistEntity::class, ['integration_id' => 'id']);
    }

    public function getTrackToIntegrations(): ActiveQueryInterface
    {
        return $this->hasMany(TrackToIntegrationEntity::class, ['integration_id' => 'id']);
    }

    public function getTracks(): ActiveQueryInterface
    {
        return $this->hasMany(TrackEntity::class, ['id' => 'track_id'])
            ->via('trackToIntegrations');
    }
}