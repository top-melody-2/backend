<?php

declare(strict_types=1);

namespace shared\domain\entity;

use shared\domain\exceptions\DomainException;
use yii\db\ActiveRecord;

abstract class AbstractEntity extends ActiveRecord
{
    final public function rules(): array
    {
       return array_map(
           fn (string $propertyName) => [$propertyName, 'safe'],
           $this->getProperties()
       );
    }

    final public function attributeLabels(): array
    {
        return [];
    }

    /**
     * @return array<string, mixed>
     */
    final public function fields(): array
    {
        return parent::fields();
    }

    /**
     * @return string[]
     */
    abstract protected function getProperties(): array;

    /**
     * @throws DomainException
     */
    final public function save($runValidation = false, $attributeNames = null): void
    {
        if (parent::save($runValidation, $attributeNames) === false) {
            $message = 'Не удалось выполнить сохранение сущности ' . static::class;
            throw new DomainException($message);
        };
    }

    final public function insert($runValidation = false, $attributes = null): void
    {
        parent::insert($runValidation, $attributes);
    }

    final public function update($runValidation = false, $attributeNames = null): void
    {
        parent::update($runValidation, $attributeNames);
    }

    public static function tableName(): string
    {
        return parent::tableName();
    }
}