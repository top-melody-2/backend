<?php

declare(strict_types=1);

namespace shared\domain\entity;

use yii\db\ActiveQueryInterface;

/**
 * @property int $id
 * @property string $request_url
 * @property int|null $response_status_code
 * @property string|null $request
 * @property string|null $response
 * @property array<string, string> $request_headers
 * @property array<string, string> $response_headers
 * @property string $execution_start_date_time
 * @property string $execution_end_date_time
 * @property int $integration_id
 * @property-read IntegrationEntity $integration
 */
class RequestLogEntity extends AbstractEntity
{
    public static function tableName(): string
    {
        return 'request_log';
    }

    /**
     * @return string[]
     */
    protected function getProperties(): array
    {
        return [
            'id',
            'request_url',
            'response_status_code',
            'request',
            'response',
            'request_headers',
            'response_headers',
            'execution_start_date_time',
            'execution_end_date_time',
            'integration_id',
        ];
    }

    public function getIntegration(): ActiveQueryInterface
    {
        return $this->hasOne(IntegrationEntity::class, ['id' => 'integration_id']);
    }
}