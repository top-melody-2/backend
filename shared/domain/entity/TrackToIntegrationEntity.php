<?php

declare(strict_types=1);

namespace shared\domain\entity;

/**
 * @property int $track_id
 * @property int $integration_id
 */
class TrackToIntegrationEntity extends AbstractEntity
{
    public static function tableName(): string
    {
        return 'track_to_integration';
    }

    /**
     * @return string[]
     */
    protected function getProperties(): array
    {
        return [
            'track_id',
            'integration_id',
        ];
    }
}