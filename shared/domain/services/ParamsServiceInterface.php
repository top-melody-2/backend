<?php

declare(strict_types=1);

namespace shared\domain\services;

use shared\domain\exceptions\DomainException;

interface ParamsServiceInterface
{

    /**
     * @throws DomainException
     */
    public function getPasswordSalt(): string;
}