<?php

declare(strict_types=1);

namespace shared\domain\repository;

use shared\domain\entity\TrackEntity;
use shared\domain\entity\TrackIntegrationConfigEntity;
use shared\domain\entity\TrackToIntegrationEntity;
use shared\domain\entity\TrackToPlaylistEntity;

interface TracksRepositoryInterface extends AbstractRepositoryInterface
{

    public function getById(int $id): ?TrackEntity;

    public function getByName(string $name): ?TrackEntity;

    public function isInPlaylist(int $trackId, int $playlistId): bool;

    public function isInIntegration(int $trackId, int $integrationId): bool;

    public function getConfig(int $trackId, int $integrationId): ?TrackIntegrationConfigEntity;

    /**
     * @param array<string, mixed> $config
     */
    public function updateConfig(int $trackId, int $integrationId, array $config): void;

    public function linkToPlaylist(int $trackId, int $playlistId): void;

    public function unlinkFromPlaylist(int $trackId, int $playlistId): void;

    public function linkToIntegration(int $trackId, int $integrationId): void;
}