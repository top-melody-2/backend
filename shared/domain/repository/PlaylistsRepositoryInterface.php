<?php

declare(strict_types=1);

namespace shared\domain\repository;

use shared\domain\entity\PlaylistEntity;

interface PlaylistsRepositoryInterface extends AbstractRepositoryInterface
{

    public function getById(int $id): ?PlaylistEntity;

    public function getByName(string $name): ?PlaylistEntity;

    /**
     * @return PlaylistEntity[]
     */
    public function getAll(): array;

    /**
     * @return PlaylistEntity[]
     */
    public function getByIntegration(int $integrationId): array;

    /**
     * @param array<string, mixed> $config
     */
    public function updateConfig(int $playlistId, array $config): void;

    public function getByIntegrationAndName(int $integrationId, string $playlistName): ?PlaylistEntity;
}