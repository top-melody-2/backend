<?php

declare(strict_types=1);

namespace shared\domain\repository;

use shared\domain\entity\AbstractEntity;
use shared\domain\exceptions\DomainException;
use yii\db\ActiveRecordInterface;

interface AbstractRepositoryInterface
{
    /**
     * @throws DomainException
     */
    public function save(AbstractEntity $entity): void;
}