<?php

declare(strict_types=1);

namespace shared\domain\repository;

interface JWTRepositoryInterface
{

    public function isRecalled(string $jwt): bool;
}