<?php

declare(strict_types=1);

namespace shared\domain\repository;

use shared\domain\entity\UserEntity;

interface UserRepositoryInterface
{

    public function getById(int $id): ?UserEntity;

    public function getByEmail(string $email): ?UserEntity;
}