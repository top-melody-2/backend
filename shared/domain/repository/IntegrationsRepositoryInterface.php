<?php

declare(strict_types=1);

namespace shared\domain\repository;

use shared\domain\entity\IntegrationEntity;
use shared\domain\entity\RequestLogEntity;

interface IntegrationsRepositoryInterface extends AbstractRepositoryInterface
{

    public function getByClass(string $class): ?IntegrationEntity;

    /**
     * @return IntegrationEntity[]
     */
    public function getAll(): array;

    public function insertRequestLog(RequestLogEntity $requestLog): void;

    /**
     * @param array<string, mixed> $config
     */
    public function updateConfig(int $integrationId, array $config): void;

    /**
     * @return string[]
     */
    public function getAllClasses(): array;
}