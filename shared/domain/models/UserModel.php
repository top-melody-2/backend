<?php

declare(strict_types=1);

namespace shared\domain\models;

use shared\domain\entity\UserEntity;
use shared\domain\repository\UserRepositoryInterface;
use shared\domain\services\ParamsServiceInterface;
use shared\domain\exceptions\DomainException;

class UserModel
{

    public function __construct(
        private UserRepositoryInterface $userRepository,
        private ParamsServiceInterface $paramsService,
    ) {}

    /**
     * @throws DomainException
     */
    public function create(string $email, string $password): void
    {
        if ($this->userRepository->getByEmail($email)) {
            throw new DomainException('Пользователь уже существует');
        }

        $user = new UserEntity();
        $user->email = $email;
        $user->password_hash = crypt($password, $this->paramsService->getPasswordSalt());

        $this->userRepository->save($user);
    }

    /**
     * @throws DomainException
     */
    public function get(string $email, string $password): UserEntity
    {
        $userEntity = $this->userRepository->getByEmail($email);
        if (!$userEntity) {
            throw new DomainException('Неверный email или пароль');
        }

        if (!password_verify($password, $userEntity->password_hash)) {
            throw new DomainException('Неверный email или пароль');
        }

        return $userEntity;
    }
}