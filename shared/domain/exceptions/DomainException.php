<?php

declare(strict_types=1);

namespace shared\domain\exceptions;

use Exception;
use Throwable;

class DomainException extends Exception
{

    public function __construct(string $message = "", int $code = 409, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}