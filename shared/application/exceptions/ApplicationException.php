<?php

declare(strict_types=1);

namespace shared\application\exceptions;

use Exception;

class ApplicationException extends Exception {}