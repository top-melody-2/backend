<?php
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@integrations', dirname(dirname(__DIR__)) . '/integrations');
Yii::setAlias('@shared', dirname(dirname(__DIR__)) . '/shared');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
