<?php

use api\domain\services\RequestServiceInterface;
use api\infrastructure\services\RequestService;
use shared\domain\repository\IntegrationsRepositoryInterface;
use shared\domain\repository\JWTRepositoryInterface;
use shared\domain\repository\PlaylistsRepositoryInterface;
use shared\domain\repository\TracksRepositoryInterface;
use shared\domain\repository\UserRepositoryInterface;
use shared\domain\services\ParamsServiceInterface;
use shared\infrastructure\repository\IntegrationsRepository;
use shared\infrastructure\repository\JWTRepository;
use shared\infrastructure\repository\PlaylistsRepository;
use shared\infrastructure\repository\TracksRepository;
use shared\infrastructure\repository\UserRepository;
use shared\infrastructure\services\ParamsService;

return [
    'aliases' => [],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru',
    'components' => [
        'cache' => [
            'class' => \yii\caching\FileCache::class,
        ],
    ],
    'container' => [
        'definitions' => [
            IntegrationsRepositoryInterface::class => IntegrationsRepository::class,
            PlaylistsRepositoryInterface::class => PlaylistsRepository::class,
            TracksRepositoryInterface::class => TracksRepository::class,
            UserRepositoryInterface::class => UserRepository::class,
            JWTRepositoryInterface::class => JWTRepository::class,
            RequestServiceInterface::class => RequestService::class,
            ParamsServiceInterface::class => ParamsService::class,
        ],
    ],
];
