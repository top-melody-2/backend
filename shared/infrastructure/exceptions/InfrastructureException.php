<?php

declare(strict_types=1);

namespace shared\infrastructure\exceptions;

use Exception;

class InfrastructureException extends Exception {}