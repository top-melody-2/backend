<?php

declare(strict_types=1);

namespace shared\infrastructure\repository;

use shared\domain\entity\IntegrationEntity;
use shared\domain\entity\RequestLogEntity;
use shared\domain\repository\IntegrationsRepositoryInterface;
use Yii;

class IntegrationsRepository extends AbstractRepository implements IntegrationsRepositoryInterface
{
    public function getByClass(string $class): ?IntegrationEntity
    {
        return IntegrationEntity::findOne(['class' => $class]);
    }

    /**
     * @return IntegrationEntity[]
     */
    public function getAll(): array
    {
        return IntegrationEntity::find()->all();
    }

    public function insertRequestLog(RequestLogEntity $requestLog): void
    {
        $requestLog->insert();
    }

    public function updateConfig(int $integrationId, array $config): void
    {
        IntegrationEntity::updateAll(
            ['config' => $config],
            ['id' => $integrationId]
        );
    }

    /**
     * @inheritDoc
     */
    public function getAllClasses(): array
    {
        return IntegrationEntity::find()->select('class')->column();
    }
}