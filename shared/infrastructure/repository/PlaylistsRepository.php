<?php

declare(strict_types=1);

namespace shared\infrastructure\repository;

use shared\domain\entity\PlaylistEntity;
use shared\domain\repository\PlaylistsRepositoryInterface;

class PlaylistsRepository extends AbstractRepository implements PlaylistsRepositoryInterface
{
    public function getById(int $id): ?PlaylistEntity
    {
        return PlaylistEntity::findOne(['id' => $id]);
    }

    public function getByName(string $name): ?PlaylistEntity
    {
        return PlaylistEntity::findOne(['name' => $name]);
    }

    /**
     * @return PlaylistEntity[]
     */
    public function getAll(): array
    {
        return PlaylistEntity::find()->all();
    }

    /**
     * @return PlaylistEntity[]
     */
    public function getByIntegration(int $integrationId): array
    {
        return PlaylistEntity::findAll(['integration_id' => $integrationId]);
    }

    public function updateConfig(int $playlistId, array $config): void
    {
        PlaylistEntity::updateAll(
            ['config' => $config],
            ['id' => $playlistId]
        );
    }

    public function getByIntegrationAndName(int $integrationId, string $playlistName): ?PlaylistEntity
    {
        return PlaylistEntity::findOne(
            ['integration_id' => $integrationId, 'name' => $playlistName]
        );
    }
}