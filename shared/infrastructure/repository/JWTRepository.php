<?php

declare(strict_types=1);

namespace shared\infrastructure\repository;

use shared\domain\entity\RecalledJWT;
use shared\domain\repository\JWTRepositoryInterface;

class JWTRepository extends AbstractRepository implements JWTRepositoryInterface
{

    public function isRecalled(string $jwt): bool
    {
        return RecalledJWT::find()
            ->where(['jwt' => $jwt])
            ->exists();
    }
}