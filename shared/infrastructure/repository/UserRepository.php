<?php

declare(strict_types=1);

namespace shared\infrastructure\repository;

use shared\domain\entity\UserEntity;
use shared\domain\repository\UserRepositoryInterface;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

    public function getById(int $id): ?UserEntity
    {
        return UserEntity::findOne(['id' => $id]);
    }

    public function getByEmail(string $email): ?UserEntity
    {
        return UserEntity::findOne(['email' => $email]);
    }
}