<?php

declare(strict_types=1);

namespace shared\infrastructure\repository;

use shared\domain\entity\TrackEntity;
use shared\domain\entity\TrackIntegrationConfigEntity;
use shared\domain\entity\TrackToIntegrationEntity;
use shared\domain\entity\TrackToPlaylistEntity;
use shared\domain\repository\TracksRepositoryInterface;

class TracksRepository extends AbstractRepository implements TracksRepositoryInterface
{
    public function getById(int $id): ?TrackEntity
    {
        return TrackEntity::findOne(['id' => $id]);
    }

    public function getByName(string $name): ?TrackEntity
    {
        return TrackEntity::findOne(['name' => $name]);
    }

    public function isInPlaylist(int $trackId, int $playlistId): bool
    {
        return TrackToPlaylistEntity::find()
            ->where([
               'track_id' => $trackId,
               'playlist_id' => $playlistId
            ])->exists();
    }

    public function isInIntegration(int $trackId, int $integrationId): bool
    {
        return TrackToIntegrationEntity::find()
            ->where([
                'track_id' => $trackId,
                'integration_id' => $integrationId
            ])->exists();
    }

    public function getConfig(int $trackId, int $integrationId): ?TrackIntegrationConfigEntity
    {
        return TrackIntegrationConfigEntity::findOne([
            'track_id' => $trackId,
            'integration_id' => $integrationId,
        ]);
    }

    public function updateConfig(int $trackId, int $integrationId, array $config): void
    {
        TrackIntegrationConfigEntity::updateAll(
            ['config' => $config],
            ['track_id' => $trackId, 'integration_id' => $integrationId]
        );
    }

    public function linkToPlaylist(int $trackId, int $playlistId): void
    {
        $trackToPlaylistEntity = new TrackToPlaylistEntity();
        $trackToPlaylistEntity->track_id = $trackId;
        $trackToPlaylistEntity->playlist_id = $playlistId;

        $trackToPlaylistEntity->insert();
    }

    public function unlinkFromPlaylist(int $trackId, int $playlistId): void
    {
        TrackToPlaylistEntity::deleteAll(
            ['track_id' => $trackId, 'playlist_id' => $playlistId]
        );
    }

    public function linkToIntegration(int $trackId, int $integrationId): void
    {
        $trackToIntegrationEntity = new TrackToIntegrationEntity();
        $trackToIntegrationEntity->track_id = $trackId;
        $trackToIntegrationEntity->integration_id = $integrationId;

        $trackToIntegrationEntity->insert();
    }
}