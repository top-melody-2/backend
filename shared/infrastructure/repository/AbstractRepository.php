<?php

declare(strict_types=1);

namespace shared\infrastructure\repository;

use shared\domain\entity\AbstractEntity;
use shared\domain\exceptions\DomainException;
use shared\domain\repository\AbstractRepositoryInterface;
use yii\db\ActiveRecordInterface;

abstract class AbstractRepository implements AbstractRepositoryInterface
{

    /**
     * @throws DomainException
     */
    public function save(AbstractEntity $entity): void
    {
        $entity->save();
    }
}