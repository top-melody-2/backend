<?php

declare(strict_types=1);

namespace shared\infrastructure\services;

use shared\domain\exceptions\DomainException;
use shared\domain\services\ParamsServiceInterface;
use shared\infrastructure\services\traits\GetParamsValueTrait;

class ParamsService implements ParamsServiceInterface
{

    use GetParamsValueTrait;

    /**
     * @throws DomainException
     */
    public function getPasswordSalt(): string
    {
        return (string) $this->getParamsValue('passwordSalt');
    }
}