<?php

declare(strict_types=1);

namespace shared\infrastructure\services\traits;

use shared\domain\exceptions\DomainException;
use Yii;

trait GetParamsValueTrait
{

    /**
     * @throws DomainException
     */
    private function getParamsValue(string $name): mixed
    {
        $value = Yii::$app->params[$name] ?? null;
        if (!$value) {
            throw new DomainException("Не заполнен параметр $name");
        }

        return $value;
    }
}