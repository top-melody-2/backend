<?php

declare(strict_types=1);

namespace shared\infrastructure\components;

use DateTimeImmutable;
use Psr\Clock\ClockInterface;

class JwtSystemClock implements ClockInterface
{

    public function now(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}