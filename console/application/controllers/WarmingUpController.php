<?php

declare(strict_types=1);

namespace console\application\controllers;

use integrations\domain\models\IntegrationsModel;
use integrations\domain\models\PlaylistsModel;
use yii\console\Controller;
use Throwable;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use shared\domain\exceptions\DomainException;
use backendIntegrationCore\domain\exceptions\NeedLogException;

class WarmingUpController extends Controller
{
    public function __construct(
                                  $id,
                                  $module,
        private IntegrationsModel $integrationsModel,
        private PlaylistsModel    $playlistsModel,
    ) {
        parent::__construct($id, $module, []);
    }

    /**
     * @throws DomainException
     * @throws IntegrationException
     * @throws Throwable
     */
    public function actionUpdateIntegrationList(): void
    {
        $this->integrationsModel->updateList();
        echo 'Список интеграций успешно обновлен', PHP_EOL;
    }

    /**
     * @throws DomainException
     * @throws IntegrationException
     * @throws Throwable
     * @throws NeedLogException
     */
    public function actionUpdatePlaylistList(): void
    {
        $this->playlistsModel->updateList();
        echo 'Список плейлистов успешно обновлен', PHP_EOL;
    }

    /**
     * @throws DomainException
     * @throws IntegrationException
     * @throws NeedLogException
     * @throws Throwable
     */
    public function actionUpdatePlaylistConfigs(): void
    {
        $this->playlistsModel->updateConfigs();
        echo 'Конфигурации плейлистов успешно обновлены', PHP_EOL;
    }
}