<?php

use yii\console\controllers\MigrateController;

$params = array_merge(
    require __DIR__ . '/../../shared/config/params.php',
    require __DIR__ . '/../../shared/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\application\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => MigrateController::class,
            'templateFile' => '@console/migrations/templates/Common.php',
        ],
    ],
    'aliases' => [],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
