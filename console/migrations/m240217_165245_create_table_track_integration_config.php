<?php

use console\migrations\components\MetaMigration;

class m240217_165245_create_table_track_integration_config extends MetaMigration
{
    private const TABLE = 'track_integration_config';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'track_id' => $this->integer()->notNull(),
            'integration_id' => $this->integer()->notNull(),
            'config' => $this->json()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY', self::TABLE, ['track_id', 'integration_id']);

        $this->addDefaultForeignKey(
            self::TABLE,
            'track_id',
            'track',
            'id'
        );

        $this->addDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id'
        );
    }

    public function down(): void
    {
        $this->dropDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id'
        );

        $this->dropDefaultForeignKey(
            self::TABLE,
            'track_id',
            'track',
            'id'
        );

        $this->dropTable(self::TABLE);
    }
}
