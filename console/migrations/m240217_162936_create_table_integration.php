<?php

use console\migrations\components\MetaMigration;

class m240217_162936_create_table_integration extends MetaMigration
{
    private const TABLE = 'integration';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull()->unique(),
            'class' => $this->string(512)->notNull()->unique(),
            'config' => $this->json()->notNull(),
        ]);
    }

    public function down(): void
    {
        $this->dropTable(self::TABLE);
    }
}
