<?php

use console\migrations\components\MetaMigration;

class m240217_164046_create_table_playlist extends MetaMigration
{
    private const TABLE = 'playlist';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'config' => $this->json()->notNull(),
            'integration_id' => $this->integer()->notNull(),
        ]);

        Yii::$app->db->createCommand('ALTER TABLE `playlist` ADD UNIQUE `unique_name_and_integration_id` (`name`, `integration_id`)')->execute();

        $this->addDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id'
        );
    }

    public function down(): void
    {
        $this->dropDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id'
        );

        $this->dropTable(self::TABLE);
    }
}
