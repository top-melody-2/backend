<?php

namespace console\migrations\components;

use yii\db\ColumnSchemaBuilder;
use yii\db\Migration;

class MetaMigration extends Migration
{
    /**
     * @param string $table
     * @param array<string, ColumnSchemaBuilder> $columns
     * @param string|null $options
     */
    public function createTable($table, $columns, $options = null): void
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        parent::createTable($table, $columns, $options);
    }

    public function addDefaultForeignKey(string $table, string $column, string $refTable, string $refColumn): void
    {
        $this->addForeignKey(
            $this->getDefaultForeignKeyName($table, $column, $refTable, $refColumn),
            $table,
            $column,
            $refTable,
            $refColumn
        );
    }

    public function dropDefaultForeignKey(string $table, string $column, string $refTable, string $refColumn): void
    {
        $this->dropForeignKey(
            $this->getDefaultForeignKeyName($table, $column, $refTable, $refColumn),
            $table
        );
    }

    private function getDefaultForeignKeyName(string $table, string $column, string $refTable, string $refColumn): string
    {
        return implode('-', [$table, $column, 'to', $refTable, $refColumn]);
    }
}
