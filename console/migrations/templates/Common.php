<?php
/* @var $className string the new migration class name */
echo "<?php\n";
?>

use console\migrations\components\MetaMigration;

class <?= $className ?> extends MetaMigration
{

    public function up(): void
    {

    }

    public function down(): void
    {

    }
}
