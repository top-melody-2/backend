<?php

use console\migrations\components\MetaMigration;

class m240512_150602_recalled_jwt extends MetaMigration
{

    private const TABLE = 'recalled_jwt';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'jwt' => $this->string()->notNull()->unique(),
            'recalled_at' => $this->dateTime()->notNull(),
        ]);
    }

    public function down(): void
    {
        $this->dropTable(self::TABLE);
    }
}
