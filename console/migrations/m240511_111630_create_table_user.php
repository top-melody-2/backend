<?php

use console\migrations\components\MetaMigration;

class m240511_111630_create_table_user extends MetaMigration
{

    private const TABLE = 'user';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
        ]);
    }

    public function down(): void
    {
        $this->dropTable(self::TABLE);
    }
}
