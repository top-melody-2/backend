<?php

use console\migrations\components\MetaMigration;

class m240217_164821_create_table_track_to_integration extends MetaMigration
{
    private const TABLE = 'track_to_integration';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'track_id' => $this->integer()->notNull(),
            'integration_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY', self::TABLE, ['track_id', 'integration_id']);

        $this->addDefaultForeignKey(
            self::TABLE,
            'track_id',
            'track',
            'id'
        );

        $this->addDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id'
        );
    }

    public function down(): void
    {
        $this->dropDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id'
        );

        $this->dropDefaultForeignKey(
            self::TABLE,
            'track_id',
            'track',
            'id'
        );

        $this->dropTable(self::TABLE);
    }
}
