<?php

use console\migrations\components\MetaMigration;

class m240217_165148_create_table_track_to_playlist extends MetaMigration
{
    private const TABLE = 'track_to_playlist';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'track_id' => $this->integer()->notNull(),
            'playlist_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY', self::TABLE, ['track_id', 'playlist_id']);

        $this->addDefaultForeignKey(
            self::TABLE,
            'track_id',
            'track',
            'id'
        );

        $this->addDefaultForeignKey(
            self::TABLE,
            'playlist_id',
            'playlist',
            'id'
        );
    }

    public function down(): void
    {
        $this->dropDefaultForeignKey(
            self::TABLE,
            'playlist_id',
            'playlist',
            'id'
        );

        $this->dropDefaultForeignKey(
            self::TABLE,
            'track_id',
            'track',
            'id'
        );

        $this->dropTable(self::TABLE);
    }
}
