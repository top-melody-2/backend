<?php

use console\migrations\components\MetaMigration;

class m240217_164237_create_table_track extends MetaMigration
{
    private const TABLE = 'track';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            // Название должно быть unique, но тогда максимальная длина поля должна составлять 191 символ
            'name' => $this->string(1024)->notNull(),
            'duration' => $this->integer()->notNull(),
        ]);
    }

    public function down(): void
    {
        $this->dropTable(self::TABLE);
    }
}
