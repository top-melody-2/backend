<?php

use console\migrations\components\MetaMigration;
use yii\db\Schema;

class m240217_164438_create_table_request_log extends MetaMigration
{
    private const TABLE = 'request_log';

    public function up(): void
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'request_url' => $this->string(2048)->notNull(),
            'response_status_code' => $this->integer(3),
            'request' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
            'response' => $this->getDb()->getSchema()->createColumnSchemaBuilder('mediumtext'),
            'request_headers' => $this->json()->notNull(),
            'response_headers' => $this->json()->notNull(),
            'execution_start_date_time' => $this->dateTime()->notNull(),
            'execution_end_date_time' => $this->dateTime()->notNull(),
            'integration_id' => $this->integer()->notNull(),
        ]);

        $this->addDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id',
        );
    }

    public function down(): void
    {
        $this->dropDefaultForeignKey(
            self::TABLE,
            'integration_id',
            'integration',
            'id',
        );

        $this->dropTable(self::TABLE);
    }
}
