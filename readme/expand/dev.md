# Локальная развёртка проекта

### Hosts
```bash
127.0.0.1 api.top-melody.local
```

### Установка прав на папку с базой данных
```bash
sudo chmod -R 0777 docker/containers/mysql/data
```

### Копирование игнорируемых гитом файлов
```bash
make init-project
```

### Запуск, остановка и перезагрузка
```bash
make start
make stop
make restart
```

### Установка vendor-пакетов
```bash
make composer-install
```

### Применение миграций
```bash
make migrate
```

### Прогрев перед запуском
```bash
make warming-up
```

### Настройка подключения к базе данных
Вкладка Database -> "+" -> Data Source -> MySQL
Host: 127.0.0.1
Port: 3306
User: tm2
Password: tm2
Database: tm2

### Настройка xDebug
- Нажмите ctrl + alt + S
- Перейдите в раздел PHP -> Servers
- Нажмите "+" (добавить новый)
- Заполните конфигурацию следующими данными:
```
Name: tm2
Host: 127.0.0.1
Port: 80
Debugger: XDebug
```
- Установите чекбокс "Use path mappings"
- Настройте соответствие между локальной папкой проекта (слева) и папкой в docker-контейнере (справа). В контейнере папка проекта - это /app