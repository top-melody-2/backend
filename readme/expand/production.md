# Настройка сервера Top Melody

<details>
<summary>Установка docker и docker-compose</summary>

docker
```bash
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Docker without sudo
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker

docker --version
```

docker-compose
```bash
# Get last released version
VER=$(curl --silent -qI https://github.com/docker/compose/releases/latest | awk -F '/' '/^location/ {print  substr($NF, 1, length($NF)-1)}')

# Download & make executable
sudo curl -L https://github.com/docker/compose/releases/download/$VER/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version
```
</details>

<details>
<summary>Firewall </summary>

Установка
```bash
sudo apt install ufw
```

Добавление разрешенных входных портов
```bash
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw allow 3306/tcp # Порт для доступа к БД
```

Запуск
```bash
sudo ufw enable
```

Установка автозапуска в момент старта системы
```bash
sudo systemctl enable ufw
```

Проверка списка открытых портов
```bash
sudo ufw status numbered
```

Удаление лишних правил
```bash
sudo ufw delete номер_правила
```
</details>

<details>
<summary>Настройка SSH</summary>

```bash
ssh-keygen -t ed25519 -C "tm2"
touch ~/.ssh/authorized_keys
cat ~/.ssh/id_ed25519.pub > ~/.ssh/authorized_keys
chmod 600 ~/.ssh/id_ed25519
```
</details>

<details>
<summary>Настройка Gitlab CI</summary>

##### Создание Variables
- Переходим в раздел Settings->CI/CD->Variables 
- Создаем 3 переменных с флагом Expanded:
```
SERVER_LOCAL_IP - IP сервера, чтобы в локальной сети резолвился на машину
SERVER_SSH_PRIVATE_KEY - приватный ключ SSH
SERVER_USER - пользователь
```

##### Настройка Runners
- Переходим в раздел Settings->CI/CD->Runners
- Снимаем чекбокс "Enable instance runners for this project"
- Нажимаем "New project runner"
- Указываем тэг "tm2"
- Нажимаем "Create runner"
- Копируем token из команды
</details>

<details>
<summary>Установка и запуск контейнера раннера на машине</summary>

Установка
```bash
docker volume create gitlab-runner-config
docker run -d --name gitlab-runner --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

Остановить Runner можно командой
```bash
docker stop gitlab-runner
```

Регистрация Runner
```bash
docker exec -it gitlab-runner gitlab-runner register  --url https://gitlab.com  --token СКОПИРОВАННЫЙ_ТОКЕН
```

После ввода команды попросят заполнить следующие поля:
- GitLab instance URL - https://gitlab.com
- Название - tm2
- Образ - alpine:latest
- Executor - docker
</details>

<details>
<summary>Разворачивание проекта</summary>

```bash
mkdir top-melody && \
git clone https://gitlab.com/top-melody-2/backend.git top-melody/backend && \
sudo apt install -y make && \
cd top-melody/backend && \
cp -R environments/prod/* . && \
make install && \
cd ../../
```
</details>