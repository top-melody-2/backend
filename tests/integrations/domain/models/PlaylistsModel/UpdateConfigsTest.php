<?php

declare(strict_types=1);

namespace tests\integrations\domain\models\PlaylistsModel;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\responses\GetPlaylistsResponseDTO;
use backendIntegrationCore\domain\IntegrationInterface;
use integrations\domain\models\PlaylistsModel;
use integrations\domain\services\EntityFiller;
use integrations\domain\services\IntegrationFactory;
use integrations\domain\services\RequestDTOBuilder;
use PHPUnit\Framework\Attributes\CoversMethod;
use PHPUnit\Framework\TestCase;
use shared\domain\entity\IntegrationEntity;
use shared\domain\entity\PlaylistEntity;
use shared\domain\exceptions\DomainException;
use shared\domain\repository\IntegrationsRepositoryInterface;
use shared\domain\repository\PlaylistsRepositoryInterface;
use shared\domain\repository\TracksRepositoryInterface;
use Yii;

#[CoversMethod(PlaylistsModel::class, 'updateConfigs')]
class UpdateConfigsTest extends TestCase
{

    public function testEmptyIntegrations(): void
    {
        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn([]);

        $playlistsRepository = $this->createMock(PlaylistsRepositoryInterface::class);
        $playlistsRepository->expects($this->never())->method('updateConfig'); // assert

        Yii::createObject(PlaylistsModel::class, [
            'integrationsRepository' => $integrationsRepository,
            'playlistsRepository' => $playlistsRepository,
        ])->updateConfigs();
    }

    public function testEmptyPlaylists(): void
    {
        $integrationEntity = new IntegrationEntity();
        $integrationEntity->id = 1;
        $integrationEntity->name = 'IntegrationName';
        $integrationEntity->class = 'IntegrationClass';

        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn([$integrationEntity]);
        $integrationsRepository->expects($this->once())->method('updateConfig'); // assert

        $integration = $this->createMock(IntegrationInterface::class);
        $integration->method('getPlaylists')->willReturn(new GetPlaylistsResponseDTO([], new ConfigCollection(), []));
        $integrationFactory = $this->createMock(IntegrationFactory::class);
        $integrationFactory->method('service')->willReturn($integration);

        $playlistsRepository = $this->createMock(PlaylistsRepositoryInterface::class);
        $playlistsRepository->expects($this->never())->method('updateConfig'); // assert

        Yii::createObject(PlaylistsModel::class, [
            'integrationFactory' => $integrationFactory,
            'integrationsRepository' => $integrationsRepository,
            'playlistsRepository' => $playlistsRepository,
        ])->updateConfigs();
    }

    public function testPlaylistNotFound(): void
    {
        $integrationEntity = new IntegrationEntity();
        $integrationEntity->id = 1;
        $integrationEntity->name = 'IntegrationName';
        $integrationEntity->class = 'IntegrationClass';

        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn([$integrationEntity]);
        $integrationsRepository->expects($this->once())->method('updateConfig'); // assert

        $integration = $this->createMock(IntegrationInterface::class);
        $integration->method('getPlaylists')
            ->willReturn(
                    new GetPlaylistsResponseDTO(
                    [],
                    new ConfigCollection(),
                    [new PlaylistDTO('p1', new ConfigCollection())]
                )
            );
        $integrationFactory = $this->createMock(IntegrationFactory::class);
        $integrationFactory->method('service')->willReturn($integration);

        $playlistsRepository = $this->createMock(PlaylistsRepositoryInterface::class);
        $playlistsRepository->method('getByIntegrationAndName')->willReturn(null);
        $playlistsRepository->expects($this->never())->method('updateConfig'); // assert

        $this->expectException(DomainException::class);

        Yii::createObject(PlaylistsModel::class, [
            'integrationFactory' => $integrationFactory,
            'integrationsRepository' => $integrationsRepository,
            'playlistsRepository' => $playlistsRepository,
        ])->updateConfigs();
    }

    public function testSuccessUpdateOneIntegrationAndManyPlaylists(): void
    {
        $integrationEntity = new IntegrationEntity();
        $integrationEntity->id = 1;
        $integrationEntity->name = 'IntegrationName';
        $integrationEntity->class = 'IntegrationClass';

        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn([$integrationEntity]);
        $integrationsRepository->expects($this->once())->method('updateConfig'); // assert

        $playlistDtoList = [
            new PlaylistDTO('p1', new ConfigCollection()),
            new PlaylistDTO('p2', new ConfigCollection())
        ];
        $integration = $this->createMock(IntegrationInterface::class);
        $integration->method('getPlaylists')->willReturn(
            new GetPlaylistsResponseDTO(
                [],
                new ConfigCollection(),
                $playlistDtoList
            )
        );
        $integrationFactory = $this->createMock(IntegrationFactory::class);
        $integrationFactory->method('service')->willReturn($integration);

        $playlistEntity = new PlaylistEntity();
        $playlistEntity->id = 1;
        $playlistsRepository = $this->createMock(PlaylistsRepositoryInterface::class);
        $playlistsRepository->method('getByIntegrationAndName')->willReturn($playlistEntity);
        $playlistsRepository->expects($this->exactly(count($playlistDtoList)))->method('updateConfig'); // assert

        Yii::createObject(PlaylistsModel::class, [
            'integrationFactory' => $integrationFactory,
            'integrationsRepository' => $integrationsRepository,
            'playlistsRepository' => $playlistsRepository,
        ])->updateConfigs();
    }
}