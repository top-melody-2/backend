<?php

declare(strict_types=1);

namespace tests\integrations\domain\models\PlaylistsModel;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\responses\GetPlaylistsResponseDTO;
use backendIntegrationCore\domain\IntegrationInterface;
use integrations\domain\models\PlaylistsModel;
use integrations\domain\services\EntityFiller;
use integrations\domain\services\IntegrationFactory;
use integrations\domain\services\RequestDTOBuilder;
use PHPUnit\Framework\Attributes\CoversMethod;
use PHPUnit\Framework\TestCase;
use shared\domain\entity\IntegrationEntity;
use shared\domain\entity\PlaylistEntity;
use shared\domain\repository\IntegrationsRepositoryInterface;
use shared\domain\repository\PlaylistsRepositoryInterface;
use shared\domain\repository\TracksRepositoryInterface;
use Yii;

#[CoversMethod(PlaylistsModel::class, 'updateList')]
class UpdateListTest extends TestCase
{

    public function testSuccessUpdateIntegrationsAndPlaylists(): void
    {
        $integration1 = new IntegrationEntity();
        $integration1->id = 1;
        $integration1->name = 'Integration1';
        $integration1->class = 'IntegrationClass1';

        $integration2 = new IntegrationEntity();
        $integration2->id = 2;
        $integration2->name = 'Integration2';
        $integration2->class = 'IntegrationClass2';

        $integrationEntities = [$integration1, $integration2];
        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn($integrationEntities);
        $integrationsRepository->expects( // assert
            $this->exactly(count($integrationEntities))
        )->method('updateConfig');

        $playlistDtoList = [
            new PlaylistDTO('Playlist1', new ConfigCollection()),
            new PlaylistDTO('Playlist2', new ConfigCollection())
        ];
        $getPlaylistsResponseDTO = new GetPlaylistsResponseDTO([], new ConfigCollection(), $playlistDtoList);
        $integration = $this->createMock(IntegrationInterface::class);
        $integration->method('getPlaylists')->willReturn($getPlaylistsResponseDTO);
        $integrationFactory = $this->createMock(IntegrationFactory::class);
        $integrationFactory->method('service')->willReturn($integration);

        $playlistEntity = new PlaylistEntity();
        $playlistEntity->id = 1;

        $playlistsRepository = $this->createMock(PlaylistsRepositoryInterface::class);
        $playlistsRepository->method('getByIntegrationAndName')->willReturn($playlistEntity);
        $playlistsRepository->expects( // assert
            $this->exactly(count($integrationEntities) * count($playlistDtoList))
        )->method('save');

        Yii::createObject(PlaylistsModel::class, [
            'integrationFactory' => $integrationFactory,
            'integrationsRepository' => $integrationsRepository,
            'playlistsRepository' => $playlistsRepository,
        ])->updateList();
    }

    public function testSuccessUpdateOneOfTwoPlaylists(): void
    {
        $integration = new IntegrationEntity();
        $integration->id = 1;
        $integration->name = 'Integration';
        $integration->class = 'IntegrationClass';

        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn([$integration]);
        $integrationsRepository->expects($this->once())->method('updateConfig'); // assert

        $playlistDtoList = [
            new PlaylistDTO('Playlist1', new ConfigCollection()),
            new PlaylistDTO('Playlist2', new ConfigCollection())
        ];
        $getPlaylistsResponseDTO = new GetPlaylistsResponseDTO([], new ConfigCollection(), $playlistDtoList);
        $integration = $this->createMock(IntegrationInterface::class);
        $integration->method('getPlaylists')->willReturn($getPlaylistsResponseDTO);
        $integrationFactory = $this->createMock(IntegrationFactory::class);
        $integrationFactory->method('service')->willReturn($integration);

        $playlistEntity = new PlaylistEntity();
        $playlistEntity->name = 'Playlist1';

        $playlistsRepository = $this->createMock(PlaylistsRepositoryInterface::class);
        $playlistsRepository->method('getByIntegration')->willReturn([$playlistEntity]);
        $playlistsRepository->expects($this->once())->method('save'); // assert

        Yii::createObject(PlaylistsModel::class, [
            'integrationFactory' => $integrationFactory,
            'integrationsRepository' => $integrationsRepository,
            'playlistsRepository' => $playlistsRepository,
        ])->updateList();
    }

    public function testNotFountIntegrations(): void
    {
        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn([]);
        $integrationsRepository->expects($this->never())->method('updateConfig'); // assert

        Yii::createObject(PlaylistsModel::class, [
            'integrationsRepository' => $integrationsRepository,
        ])->updateList();
    }

    public function testNotFoundIntegrationPlaylists(): void
    {
        $integration = new IntegrationEntity();
        $integration->id = 1;
        $integration->name = 'Integration1';
        $integration->class = 'IntegrationClass1';

        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAll')->willReturn([$integration]);
        $integrationsRepository->expects($this->once())->method('updateConfig'); // assert

        $integration = $this->createMock(IntegrationInterface::class);
        $integration->method('getPlaylists')->willReturn(
            new GetPlaylistsResponseDTO(
                [],
                new ConfigCollection(),
                []
            )
        );
        $integrationFactory = $this->createMock(IntegrationFactory::class);
        $integrationFactory->method('service')->willReturn($integration);

        $playlistsRepository = $this->createMock(PlaylistsRepositoryInterface::class);
        $playlistsRepository->expects($this->never())->method('save'); // assert

        Yii::createObject(PlaylistsModel::class, [
            'integrationFactory' => $integrationFactory,
            'integrationsRepository' => $integrationsRepository,
            'playlistsRepository' => $playlistsRepository,
        ])->updateList();
    }
}