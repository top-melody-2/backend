<?php

declare(strict_types=1);

namespace tests\integrations\domain\models\IntegrationsModel;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\responses\GetInfoResponseDTO;
use backendIntegrationCore\domain\IntegrationInterface;
use integrations\domain\IntegrationsCollection;
use integrations\domain\models\IntegrationsModel;
use integrations\domain\services\EntityFiller;
use integrations\domain\services\IntegrationFactory;
use PHPUnit\Framework\Attributes\CoversMethod;
use PHPUnit\Framework\TestCase;
use shared\domain\entity\IntegrationEntity;
use shared\domain\repository\IntegrationsRepositoryInterface;
use Yii;

#[CoversMethod(IntegrationsModel::class, 'updateList')]
class UpdateListTest extends TestCase
{

    public function testEmptyDatabase(): void
    {
        $integrationsCount = count(Yii::createObject(IntegrationsCollection::class)->getAllClasses());

        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAllClasses')->willReturn([]);
        $integrationsRepository->expects($this->exactly($integrationsCount))->method('save'); // assert

        Yii::createObject(IntegrationsModel::class, [
            'integrationsRepository' => $integrationsRepository
        ])->updateList();
    }

    public function testNothingToUpdate(): void
    {
        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAllClasses')
            ->willReturn(Yii::createObject(IntegrationsCollection::class)->getAllClasses());
        $integrationsRepository->expects($this->never())->method('save'); // assert

        Yii::createObject(IntegrationsModel::class, [
            'integrationsRepository' => $integrationsRepository
        ])->updateList();
    }

    public function testUpdateOneOfTwoIntegrations(): void
    {
        $integrationsRepository = $this->createMock(IntegrationsRepositoryInterface::class);
        $integrationsRepository->method('getAllClasses')->willReturn(['i1']);
        $integrationsRepository->expects($this->once())->method('save'); // assert

        $integrationsCollection = $this->createMock(IntegrationsCollection::class);
        $integrationsCollection->method('getAllClasses')->willReturn(['i1', 'i2']);

        $integration = $this->createMock(IntegrationInterface::class);
        $integration->method('getInfo')->willReturn(
            new GetInfoResponseDTO(
                new IntegrationDTO('i2', new ConfigCollection())
            )
        );
        $integrationFactory = $this->createMock(IntegrationFactory::class);
        $integrationFactory->method('service')->willReturn($integration);

        Yii::createObject(IntegrationsModel::class, [
            'integrationsRepository' => $integrationsRepository,
            'integrationFactory' => $integrationFactory,
            'integrationsCollection' => $integrationsCollection,
        ])->updateList();
    }
}