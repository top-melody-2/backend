<?php
define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);
include_once __DIR__.'/../vendor/autoload.php';


require_once(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../shared/config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../shared/config/main.php'),
    require(__DIR__ . '/../shared/config/main-local.php'),
    require(__DIR__ . '/../api/config/main.php'),
    require(__DIR__ . '/../api/config/main-local.php'),
);

$GLOBALS['config'] = $config;

Yii::setAlias('@tests', __DIR__);
$WEB_MODULE = new yii\web\Application($config);
